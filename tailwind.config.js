const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Source Sans Pro", "sans-serif"],
      serif: ["Roboto Slab", "serif"],
    },
    extend: {
      fontWeight: ["hover", "focus"],
      colors: {
        primary: "#38EA85",
        "secondary-1": "#3FAEE4",
        "secondary-2": "#A0F38B",
        complementary: "#FF653D",
        "bg-light": colors.gray["100"],
        "bg-dark": colors.gray["800"],
        light: colors.gray["200"],
        dark: colors.gray["800"],
      },
    },
  },
  variants: {
    extend: {
      transitionProperty: ["hover", "focus"],
    },
  },
  plugins: [],
};
