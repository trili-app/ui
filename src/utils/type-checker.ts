import {
  LocalizedTopic_localizedTopic_connections_content,
  LocalizedTopic_localizedTopic_connections_content_MarkdownContent,
} from "../views/__generated__/LocalizedTopic";
import {
  Template_template_children,
  Template_template_children_HeaderElement,
  Template_template_children_MarkdownElement,
  Template_template_children_StackElement,
  Template_template_children_TableElement,
} from "../views/__generated__/Template";

export const tryMatchStack = (
  element: Template_template_children
): Template_template_children_StackElement | false =>
  element.__typename === "StackElement" ? element : false;

export const tryMatchHeader = (
  element: Template_template_children
): Template_template_children_HeaderElement | false =>
  element.__typename === "HeaderElement" ? element : false;

export const tryMatchMarkdown = (
  element: Template_template_children
): Template_template_children_MarkdownElement | false =>
  element.__typename === "MarkdownElement" ? element : false;

export const tryMatchTable = (
  element: Template_template_children
): Template_template_children_TableElement | false =>
  element.__typename === "TableElement" ? element : false;

export const tryMatchingMarkdown = (
  c: LocalizedTopic_localizedTopic_connections_content | null
): LocalizedTopic_localizedTopic_connections_content_MarkdownContent | false =>
  c?.__typename === "MarkdownContent" ? c : false;
