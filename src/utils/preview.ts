import { Template_template_children } from "../views/__generated__/Template";

export const buildElementTree = (
  elements: Array<Template_template_children>
): Map<string, Array<Template_template_children>> => {
  const assignments: Map<string, Array<Template_template_children>> = new Map();
  elements
    .filter((n) => n.parentId !== null)
    .forEach((e) => {
      const parentId = e.parentId!;
      const children = assignments.get(parentId) || [];
      children.push(e);
      assignments.set(parentId, children);
    });

  return assignments;
};
