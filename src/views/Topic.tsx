import {
  Component,
  createComputed,
  createEffect,
  createSignal,
  Match,
  Switch,
} from "solid-js";
import { useParams } from "solid-app-router";
import { gql, OperationResult } from "@urql/core";
import TopicPage from "./topic/TopicPage";
import Page from "../components/layout/Page";
import { useUrql } from "../contexts/urql";
import {
  LocalizedTopic,
  LocalizedTopicVariables,
} from "./__generated__/LocalizedTopic";
import {
  LocalizedTopicProvider,
  useLocalizedTopic,
} from "../contexts/localized-topic";

const LocalizedTopicQuery = gql`
  query LocalizedTopic($id: ID!) {
    localizedTopic(id: $id) {
      id
      name
      language
      topic {
        id
        template {
          identifier
          description
          children {
            __typename
            id
            parentId
            ... on MarkdownElement {
              identifier
            }
            ... on TableElement {
              headers
            }
            ... on StackElement {
              direction
            }
            ... on HeaderElement {
              level
              textIdentifier
              resolvedText {
                identifier
                text
                language
              }
            }
          }
        }
        connections {
          id
          schema {
            identifier
          }
          content {
            __typename
            ... on DateContent {
              id
              date
            }
            ... on NumericContent {
              id
              value
            }
          }
        }
      }
      connections {
        id
        schema {
          identifier
        }
        content {
          __typename
          ... on MarkdownContent {
            id
            beginner
            advanced
            expert
          }
          ... on TextContent {
            identifier
            text
          }
        }
      }
    }
  }
`;

const TopicComponent: Component = () => {
  const { urql } = useUrql();

  const fetchLocalizedTopic = (id: string) =>
    urql
      .query<LocalizedTopic, LocalizedTopicVariables>(LocalizedTopicQuery, {
        id,
      })
      .toPromise();
  const params = useParams<{ id: string }>();
  const [result, setResult] =
    createSignal<OperationResult<LocalizedTopic> | null>(null);

  createComputed(async () => {
    const res = await fetchLocalizedTopic(params.id);
    setResult(res);
  });

  return (
    <Page>
      <div class="max-w-7xl mx-auto py-6 px-2 lg:px-8">
        <Switch>
          {/* Loading */}
          <Match when={!result()?.data}>Loading...</Match>
          {/* Page not found */}
          <Match when={result()?.data?.localizedTopic === null}>
            Topic not found.
          </Match>
          {/* Page */}
          <Match when={result()!.data}>
            <LocalizedTopicProvider topic={result()?.data?.localizedTopic!}>
              <TopicPage localizedTopic={result()?.data?.localizedTopic!} />
            </LocalizedTopicProvider>
          </Match>
        </Switch>
      </div>
    </Page>
  );
};

export default TopicComponent;
