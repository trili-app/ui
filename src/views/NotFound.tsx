import { Component } from "solid-js";
import Header from "../components/typography/Header";
import Page from "../components/layout/Page";

const NotFound: Component = () => {
  return (
    <Page>
      <Header level={1}>Not Found</Header>
      <p>This page seems to be missing. :(</p>
    </Page>
  );
};

export default NotFound;
