import { Component } from "solid-js";
import Logo from "../components/Logo";
import SearchInput from "../components/inputs/SearchInput";
import Footer from "../components/layout/Footer";

const Home: Component = () => {
  return (
    <div class="flex flex-col min-h-screen justify-between bg-bg-light">
      <main class="mb-auto">
        <div class="mx-2 py-6 sm:px-6 lg:px-8">
          <div class="flex flex-col justify-between items-center gap-6 lg:mt-12">
            <Logo />
            <div class="w-full md:w-1/2">
              <SearchInput />
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default Home;
