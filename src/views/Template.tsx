import { Component, createEffect, createSignal, Match, Switch } from "solid-js";
import { useParams } from "solid-app-router";
import { gql, OperationResult } from "@urql/core";
import TemplatePage from "./template/TemplatePage";
import Page from "../components/layout/Page";
import { useUrql } from "../contexts/urql";
import { Template, TemplateVariables } from "./__generated__/Template";

const TemplateQuery = gql`
  query Template($id: ID!) {
    template(id: $id) {
      identifier
      description
      children {
        __typename
        id
        parentId
        ... on MarkdownElement {
          identifier
        }
        ... on TableElement {
          headers
        }
        ... on StackElement {
          direction
        }
        ... on HeaderElement {
          level
          textIdentifier
          resolvedText {
            identifier
            text
            language
          }
        }
      }
    }
  }
`;

const TemplateComponent: Component = () => {
  const { urql } = useUrql();

  const fetchTemplate = (id: string) => {
    return urql
      .query<Template, TemplateVariables>(TemplateQuery, {
        id,
      })
      .toPromise();
  };
  const params = useParams<{ id: string }>();
  const [result, setResult] = createSignal<OperationResult<Template> | null>(
    null
  );

  createEffect(async () => {
    const res = await fetchTemplate(params.id);
    setResult(res);
  });

  return (
    <Page>
      <div class="max-w-7xl mx-auto py-6 px-2 lg:px-8">
        <Switch>
          {/* Loading */}
          <Match when={!result()?.data}>Loading...</Match>
          {/* Page not found */}
          <Match when={result()?.data?.template === null}>
            Topic not found.
          </Match>
          {/* Page */}
          <Match when={result()!.data}>
            <TemplatePage template={result()?.data?.template!} />
          </Match>
        </Switch>
      </div>
    </Page>
  );
};

export default TemplateComponent;
