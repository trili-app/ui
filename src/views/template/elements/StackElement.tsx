import { Component, Show, For } from "solid-js";
import {
  Template_template_children,
  Template_template_children_StackElement,
} from "../../__generated__/Template";
import Element from "./Element";

interface Props {
  element: Template_template_children_StackElement;
  stackChildren: Array<Template_template_children>;
  assignments: Map<string, Array<Template_template_children>>;
}

const StackElement: Component<Props> = (props) => {
  const children = props.stackChildren;
  return (
    <Show when={children.length !== 0}>
      <div class="flex flex-col gap-2 mt-4">
        <For each={children}>
          {(item) => (
            <Element
              element={item}
              assignments={props.assignments}
              isMovable={children.length > 1}
            />
          )}
        </For>
      </div>
    </Show>
  );
};

export default StackElement;
