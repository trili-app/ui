import { Component, Show, Switch, Match, createSignal } from "solid-js";
import { Icon } from "solid-heroicons";
import { switchVertical, trash, pencil } from "solid-heroicons/outline";
import { chevronRight } from "solid-heroicons/solid";
import { Template_template_children } from "../../__generated__/Template";
import Button from "../../../components/inputs/Button";
import StackElement from "./StackElement";
import {
  tryMatchStack,
  tryMatchHeader,
  tryMatchMarkdown,
} from "../../../utils/type-checker";
import { Direction } from "../../../__generated__/global";
import Dialog from "../../../components/Dialog";
import Paragraph from "../../../components/typography/Paragraph";

interface Props {
  isMovable?: boolean;
  element: Template_template_children;
  assignments: Map<string, Array<Template_template_children>>;
}

const Element: Component<Props> = (props) => {
  const children = props.assignments.get(props.element.id) || [];
  const [isExpanded, setExpanded] = createSignal(
    props.element.parentId === null
  );
  const isExpandable = !!tryMatchStack(props.element);
  const [showDetails, setShowDetails] = createSignal(false);
  return (
    <>
      <div class="p-3 border-gray-300 border rounded">
        <div class="flex justify-between items-center">
          <div>
            <div
              classList={{
                "rounded py-1 pl-2": true,
                "transition hover:text-black hover:cursor-pointer hover:bg-gray-200":
                  isExpandable,
                "pr-2": !isExpandable,
                "inline font-extrabold": true,
              }}
              onClick={
                isExpandable ? () => setExpanded(!isExpanded()) : undefined
              }
            >
              {props.element.__typename}
              <Show when={isExpandable}>
                <Icon
                  path={chevronRight}
                  classList={{
                    "h-6 inline transition origin-center": true,
                    "rotate-0": !isExpanded(),
                    "rotate-90 -translate-x-1 translate-y-0.5": isExpanded(),
                  }}
                />
              </Show>
            </div>
            <div class="inline px-1 font-light text-sm text-gray-400">
              (
              <Switch>
                <Match when={tryMatchStack(props.element)}>
                  {(item) =>
                    item.direction === Direction.HORIZONTAL
                      ? "horizontal"
                      : "vertical"
                  }
                </Match>
                <Match when={tryMatchHeader(props.element)}>
                  {(item) => `h${item.level}: "${item.textIdentifier}"`}
                </Match>
                <Match when={tryMatchMarkdown(props.element)}>
                  {(item) => `content: "${item.identifier}"`}
                </Match>
              </Switch>
              )
            </div>
          </div>
          <div class="flex gap-1">
            <Show when={props.isMovable || false}>
              <Button>
                <Icon path={switchVertical} class="inline h-4" />
              </Button>
            </Show>
            <Button onClick={() => setShowDetails(true)}>
              <Icon path={pencil} class="inline h-4" />
            </Button>
            <Button>
              <Icon path={trash} class="inline h-4 text-red-600" />
            </Button>
          </div>
        </div>
        <Show when={isExpanded()}>
          <Switch>
            <Match when={tryMatchStack(props.element)}>
              {(item) => (
                <StackElement
                  element={item}
                  assignments={props.assignments}
                  stackChildren={children}
                />
              )}
            </Match>
          </Switch>
        </Show>
      </div>

      <Dialog
        header={props.element.__typename}
        show={showDetails()}
        onClose={() => setShowDetails(false)}
      >
        <Paragraph>Fill this modal with properties later.</Paragraph>
      </Dialog>
    </>
  );
};

export default Element;
