import { Component, Show, For } from "solid-js";
import { Direction } from "../../../__generated__/global";
import {
  Template_template_children,
  Template_template_children_StackElement,
} from "../../__generated__/Template";
import Preview from "./Preview";

interface Props {
  element: Template_template_children_StackElement;
  stackChildren: Array<Template_template_children>;
  assignments: Map<string, Array<Template_template_children>>;
}

const StackPreview: Component<Props> = (props) => {
  const children = props.stackChildren;
  const isVertical = props.element.direction === Direction.VERTICAL;
  return (
    <Show when={children.length > 0}>
      <div
        classList={{
          "grid gap-2": true,
          "grid-flow-row auto-rows-max": isVertical,
          "grid-flow-col auto-cols-fr": !isVertical,
        }}
      >
        <For each={children}>
          {(item) => (
            <div>
              <Preview element={item} assignments={props.assignments} />
            </div>
          )}
        </For>
      </div>
    </Show>
  );
};

export default StackPreview;
