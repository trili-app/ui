import {
  Component,
  createComputed,
  createSignal,
  Match,
  Switch,
} from "solid-js";
import Paragraph from "../../../components/typography/Paragraph";
import { useLocalizedTopic } from "../../../contexts/localized-topic";
import { tryMatchingMarkdown } from "../../../utils/type-checker";
import { LocalizedTopic_localizedTopic_connections_content_MarkdownContent } from "../../__generated__/LocalizedTopic";
import { Template_template_children_MarkdownElement } from "../../__generated__/Template";

interface Props {
  element: Template_template_children_MarkdownElement;
}

const MarkdownPreview: Component<Props> = (props) => {
  const [topic] = useLocalizedTopic();
  const [content, setContent] =
    createSignal<LocalizedTopic_localizedTopic_connections_content_MarkdownContent | null>(
      null
    );
  createComputed(() => {
    setContent(
      (topic?.topic?.connections
        .filter((c) => c.schema.identifier === props.element.identifier)
        .map((c) => c.content)[0] ||
        null) as LocalizedTopic_localizedTopic_connections_content_MarkdownContent | null
    );
  });
  return (
    <>
      <Switch>
        <Match when={tryMatchingMarkdown(content())}>
          {(c) => (
            <Switch>
              <Match when={topic.level === "beginner"}>
                <Paragraph>{c.beginner}</Paragraph>
              </Match>
              <Match when={topic.level === "advanced"}>
                <Paragraph>{c.advanced}</Paragraph>
              </Match>
              <Match when={topic.level === "expert"}>
                <Paragraph>{c.expert}</Paragraph>
              </Match>
            </Switch>
          )}
        </Match>
        <Match when={content() === null}>
          <Paragraph>
            Missing content for "{props.element.identifier}"
          </Paragraph>
        </Match>
      </Switch>
    </>
  );
};

export default MarkdownPreview;
