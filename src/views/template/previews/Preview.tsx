import { Component, Switch, Match } from "solid-js";
import {
  tryMatchHeader,
  tryMatchMarkdown,
  tryMatchStack,
  tryMatchTable,
} from "../../../utils/type-checker";
import { Template_template_children } from "../../__generated__/Template";
import HeaderPreview from "./HeaderPreview";
import MarkdownPreview from "./MarkdownPreview";
import StackPreview from "./StackPreview";

interface Props {
  element: Template_template_children;
  assignments: Map<string, Array<Template_template_children>>;
}

const Preview: Component<Props> = (props) => {
  const children = props.assignments.get(props.element.id) || [];
  return (
    <>
      <Switch>
        <Match when={tryMatchStack(props.element)}>
          {(item) => (
            <StackPreview
              element={item}
              assignments={props.assignments}
              stackChildren={children}
            />
          )}
        </Match>
        <Match when={tryMatchHeader(props.element)}>
          {(item) => <HeaderPreview element={item} />}
        </Match>
        <Match when={tryMatchMarkdown(props.element)}>
          {(item) => <MarkdownPreview element={item} />}
        </Match>
        <Match when={tryMatchTable(props.element)}>
          {(item) => item.headers}
        </Match>
      </Switch>
    </>
  );
};

export default Preview;
