import { Component } from "solid-js";
import Header from "../../../components/typography/Header";
import { Template_template_children_HeaderElement } from "../../__generated__/Template";

interface Props {
  element: Template_template_children_HeaderElement;
}

const HeaderPreview: Component<Props> = (props) => {
  return (
    <Header level={props.element.level}>
      {props.element.resolvedText?.text}
    </Header>
  );
};

export default HeaderPreview;
