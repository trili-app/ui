import { Component, For } from "solid-js";
import { Icon } from "solid-heroicons";
import { eye, adjustments, pencil } from "solid-heroicons/outline";
import Header from "../../components/typography/Header";
import Paragraph from "../../components/typography/Paragraph";
import { Template_template } from "../__generated__/Template";
import Element from "./elements/Element";
import Preview from "./previews/Preview";
import Button from "../../components/inputs/Button";
import { buildElementTree } from "../../utils/preview";
import { LocalizedTopicProvider } from "../../contexts/localized-topic";

interface Props {
  template: Template_template;
}

const TemplatePage: Component<Props> = (props) => {
  const assignments = buildElementTree(props.template.children);
  return (
    <>
      <Header level={1}>
        Template:{" "}
        <span class="font-sans text-2xl text-gray-500">
          {props.template.identifier}
        </span>
      </Header>
      <Header level={2}>Description</Header>
      <div class="flex gap-1 justify-between items-start">
        <Paragraph>{props.template.description}</Paragraph>
        <Button>
          <Icon path={pencil} class="inline h-4" />
        </Button>
      </div>
      <Header level={2}>Structure</Header>
      <div class="grid grid-cols-2 gap-8">
        <div>
          <Header level={3}>
            Layout <Icon path={adjustments} class="inline h-4" />
          </Header>
          <For
            each={props.template.children.filter((n) => n.parentId === null)}
          >
            {(item) => <Element element={item} assignments={assignments} />}
          </For>
        </div>
        <div>
          <Header level={3}>
            Preview <Icon path={eye} class="inline h-4" />
          </Header>
          <div class="p-2 border-gray-300 border border-dotted rounded">
            <LocalizedTopicProvider topic={null}>
              <For
                each={props.template.children.filter(
                  (n) => n.parentId === null
                )}
              >
                {(item) => <Preview element={item} assignments={assignments} />}
              </For>
            </LocalizedTopicProvider>
          </div>
        </div>
      </div>
    </>
  );
};

export default TemplatePage;
