/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Language, Direction } from "./../../__generated__/global";

// ====================================================
// GraphQL query operation: LocalizedTopic
// ====================================================

export interface LocalizedTopic_localizedTopic_topic_template_children_MarkdownElement {
  __typename: "MarkdownElement";
  id: string;
  parentId: string | null;
  identifier: string;
}

export interface LocalizedTopic_localizedTopic_topic_template_children_TableElement {
  __typename: "TableElement";
  id: string;
  parentId: string | null;
  headers: string[];
}

export interface LocalizedTopic_localizedTopic_topic_template_children_StackElement {
  __typename: "StackElement";
  id: string;
  parentId: string | null;
  direction: Direction;
}

export interface LocalizedTopic_localizedTopic_topic_template_children_HeaderElement_resolvedText {
  __typename: "TextContent";
  identifier: string;
  text: string;
  language: Language;
}

export interface LocalizedTopic_localizedTopic_topic_template_children_HeaderElement {
  __typename: "HeaderElement";
  id: string;
  parentId: string | null;
  level: number;
  textIdentifier: string;
  resolvedText: LocalizedTopic_localizedTopic_topic_template_children_HeaderElement_resolvedText | null;
}

export type LocalizedTopic_localizedTopic_topic_template_children = LocalizedTopic_localizedTopic_topic_template_children_MarkdownElement | LocalizedTopic_localizedTopic_topic_template_children_TableElement | LocalizedTopic_localizedTopic_topic_template_children_StackElement | LocalizedTopic_localizedTopic_topic_template_children_HeaderElement;

export interface LocalizedTopic_localizedTopic_topic_template {
  __typename: "Template";
  identifier: string;
  description: string;
  children: LocalizedTopic_localizedTopic_topic_template_children[];
}

export interface LocalizedTopic_localizedTopic_topic_connections_schema {
  __typename: "ConnectionSchema";
  identifier: string;
}

export interface LocalizedTopic_localizedTopic_topic_connections_content_MarkdownContent {
  __typename: "MarkdownContent" | "TextContent";
}

export interface LocalizedTopic_localizedTopic_topic_connections_content_DateContent {
  __typename: "DateContent";
  id: string;
  date: any;
}

export interface LocalizedTopic_localizedTopic_topic_connections_content_NumericContent {
  __typename: "NumericContent";
  id: string;
  value: number;
}

export type LocalizedTopic_localizedTopic_topic_connections_content = LocalizedTopic_localizedTopic_topic_connections_content_MarkdownContent | LocalizedTopic_localizedTopic_topic_connections_content_DateContent | LocalizedTopic_localizedTopic_topic_connections_content_NumericContent;

export interface LocalizedTopic_localizedTopic_topic_connections {
  __typename: "Connection";
  id: string;
  schema: LocalizedTopic_localizedTopic_topic_connections_schema;
  content: LocalizedTopic_localizedTopic_topic_connections_content;
}

export interface LocalizedTopic_localizedTopic_topic {
  __typename: "Topic";
  id: string;
  template: LocalizedTopic_localizedTopic_topic_template;
  connections: LocalizedTopic_localizedTopic_topic_connections[];
}

export interface LocalizedTopic_localizedTopic_connections_schema {
  __typename: "ConnectionSchema";
  identifier: string;
}

export interface LocalizedTopic_localizedTopic_connections_content_NumericContent {
  __typename: "NumericContent" | "DateContent";
}

export interface LocalizedTopic_localizedTopic_connections_content_MarkdownContent {
  __typename: "MarkdownContent";
  id: string;
  beginner: string | null;
  advanced: string | null;
  expert: string | null;
}

export interface LocalizedTopic_localizedTopic_connections_content_TextContent {
  __typename: "TextContent";
  identifier: string;
  text: string;
}

export type LocalizedTopic_localizedTopic_connections_content = LocalizedTopic_localizedTopic_connections_content_NumericContent | LocalizedTopic_localizedTopic_connections_content_MarkdownContent | LocalizedTopic_localizedTopic_connections_content_TextContent;

export interface LocalizedTopic_localizedTopic_connections {
  __typename: "Connection";
  id: string;
  schema: LocalizedTopic_localizedTopic_connections_schema;
  content: LocalizedTopic_localizedTopic_connections_content;
}

export interface LocalizedTopic_localizedTopic {
  __typename: "LocalizedTopic";
  id: string;
  name: string;
  language: Language;
  topic: LocalizedTopic_localizedTopic_topic;
  connections: LocalizedTopic_localizedTopic_connections[];
}

export interface LocalizedTopic {
  localizedTopic: LocalizedTopic_localizedTopic | null;
}

export interface LocalizedTopicVariables {
  id: string;
}
