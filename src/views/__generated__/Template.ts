/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Direction, Language } from "./../../__generated__/global";

// ====================================================
// GraphQL query operation: Template
// ====================================================

export interface Template_template_children_MarkdownElement {
  __typename: "MarkdownElement";
  id: string;
  parentId: string | null;
  identifier: string;
}

export interface Template_template_children_TableElement {
  __typename: "TableElement";
  id: string;
  parentId: string | null;
  headers: string[];
}

export interface Template_template_children_StackElement {
  __typename: "StackElement";
  id: string;
  parentId: string | null;
  direction: Direction;
}

export interface Template_template_children_HeaderElement_resolvedText {
  __typename: "TextContent";
  identifier: string;
  text: string;
  language: Language;
}

export interface Template_template_children_HeaderElement {
  __typename: "HeaderElement";
  id: string;
  parentId: string | null;
  level: number;
  textIdentifier: string;
  resolvedText: Template_template_children_HeaderElement_resolvedText | null;
}

export type Template_template_children = Template_template_children_MarkdownElement | Template_template_children_TableElement | Template_template_children_StackElement | Template_template_children_HeaderElement;

export interface Template_template {
  __typename: "Template";
  identifier: string;
  description: string;
  children: Template_template_children[];
}

export interface Template {
  template: Template_template | null;
}

export interface TemplateVariables {
  id: string;
}
