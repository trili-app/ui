import { Component, createComputed, For } from "solid-js";
import { LocalizedTopic_localizedTopic } from "../__generated__/LocalizedTopic";
import Link from "../../components/typography/Link";
import Preview from "../template/previews/Preview";
import { buildElementTree } from "../../utils/preview";
import Header from "../../components/typography/Header";
import { Level, useLocalizedTopic } from "../../contexts/localized-topic";
import { useSearchParams } from "solid-app-router";
import Button from "../../components/inputs/Button";

interface Props {
  localizedTopic: LocalizedTopic_localizedTopic;
}

const TopicPage: Component<Props> = (props) => {
  const templateIdentifier = props.localizedTopic.topic.template.identifier;
  const assignments = buildElementTree(
    props.localizedTopic.topic.template.children
  );
  const [topic, { setTopic, setLevel }] = useLocalizedTopic();
  const [searchParams, setSearchParams] = useSearchParams<{ level?: string }>();
  const updateLevel = (level: Level) => {
    setSearchParams({ level });
    setLevel(level);
  };
  createComputed(() => {
    const level =
      searchParams.level === "expert" ||
      searchParams.level === "advanced" ||
      searchParams.level === "beginner"
        ? searchParams.level
        : "beginner";
    setLevel(level);
    setTopic(props.localizedTopic);
  });
  return (
    <>
      <div class="flex justify-between items-start mb-6">
        <Header level={1}>{props.localizedTopic.name}</Header>
        <div class="inline-grid grid-flow-col gap-1">
          <Button
            onClick={() => updateLevel("beginner")}
            classList={{
              "border border-bg-dark bg-secondary-1":
                topic.level === "beginner",
            }}
          >
            Beginner
          </Button>
          <Button
            onClick={() => updateLevel("advanced")}
            classList={{
              "border border-bg-dark": topic.level === "advanced",
            }}
          >
            Advanced
          </Button>
          <Button
            onClick={() => updateLevel("expert")}
            classList={{
              "border border-bg-dark": topic.level === "expert",
            }}
          >
            Expert
          </Button>
        </div>
        <div class="text-right">
          <div class="text-gray-500 font-serif">Template</div>
          <div class="text-sm">
            <Link href={`/en/template/${templateIdentifier}`}>
              {templateIdentifier}
            </Link>
          </div>
        </div>
      </div>
      <For
        each={props.localizedTopic.topic.template.children.filter(
          (n) => n.parentId === null
        )}
      >
        {(item) => <Preview element={item} assignments={assignments} />}
      </For>
    </>
  );
};

export default TopicPage;
