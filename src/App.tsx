import { Component, lazy } from "solid-js";
import { RouteDefinition, Router, useRoutes } from "solid-app-router";
import { UrqlProvider } from "./contexts/urql";

const routes: Array<RouteDefinition> = [
  {
    path: "/",
    component: lazy(() => import("./views/Root")),
  },
  {
    path: "/:lang/",
    component: lazy(() => import("./views/Home")),
  },
  {
    path: "/:lang/topic/:id",
    component: lazy(() => import("./views/Topic")),
  },
  {
    path: "/:lang/template/:id",
    component: lazy(() => import("./views/Template")),
  },
  {
    path: "*",
    component: lazy(() => import("./views/NotFound")),
  },
];

const App: Component = () => {
  const Routes = useRoutes(routes);

  return (
    <div class="text-dark">
      <UrqlProvider>
        <Router>
          <Routes />
        </Router>
      </UrqlProvider>
    </div>
  );
};

export default App;
