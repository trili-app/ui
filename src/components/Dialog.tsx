import { Icon } from "solid-heroicons";
import { xCircle } from "solid-heroicons/outline";
import { Component } from "solid-js";
import { Portal } from "solid-js/web";
import Button from "./inputs/Button";
import Header from "./typography/Header";

interface Props {
  show: boolean;
  header: string;
  onClose: () => void;
  isDark?: boolean;
  size?: "small" | "regular";
}

const Dialog: Component<Props> = (props) => {
  return (
    <Portal>
      <div
        classList={{
          hidden: !props.show,
          "fixed inset-0 bg-gray-600 bg-opacity-80 overflow-y-auto h-full w-full":
            true,
        }}
      >
        <div class="relative top-20 mx-auto p-5 border w-96 shadow-lg rounded-md bg-white">
          <div class="flex justify-between items-start">
            <Header level={2}>{props.header}</Header>
            <Button onClick={() => props.onClose()}>
              <Icon path={xCircle} class="inline h-6" />
            </Button>
          </div>
          {props.children}
        </div>
      </div>
    </Portal>
  );
};

export default Dialog;
