import { Component } from "solid-js";
import Footer from "./Footer";
import Nav from "./Nav";

const Page: Component = (props) => {
  return (
    <div class="flex flex-col min-h-screen justify-between bg-bg-light">
      <Nav />
      <main class="pt-14 mb-auto">{props.children}</main>
      <Footer />
    </div>
  );
};

export default Page;
