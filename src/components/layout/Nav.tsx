import { Link } from "solid-app-router";
import { Component } from "solid-js";
import SearchInput from "../inputs/SearchInput";
import Logo from "../Logo";

const Nav: Component = () => {
  return (
    <nav class="bg-bg-dark text-light flex px-3 py-3 fixed top-0 w-full h-14 shadow-lg">
      <div class="flex items-center gap-3">
        <div>
          <Link href="/en/">
            <Logo size="small" isDark />
          </Link>
        </div>
        <div>
          <SearchInput size="small" />
        </div>
        <div />
      </div>
    </nav>
  );
};

export default Nav;
