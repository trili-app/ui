import { Component } from "solid-js";
import Link from "../typography/Link";

const Footer: Component = () => {
  return (
    <footer class="flex flex-col">
      <div class="bg-bg-dark p-2 text-light text-xs">
        <div class="grid grid-cols-3 gap-4 m-2">
          <div class="flex gap-4">
            <div>
              <Link href="#">About</Link>
            </div>
            <div>
              <Link href="#">Contact</Link>
            </div>
            <div>
              <Link href="#">Privacy</Link>
            </div>
          </div>
          <div class="text-center">Made in Erfurt</div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
