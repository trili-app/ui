import { Component, JSX } from "solid-js";

type Props = {
  kind?: "primary" | "secondary";
  size?: "small" | "regular";
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const Button: Component<Props> = (props) => {
  const kind = props.kind || "secondary";
  const size = props.size || "small";
  return (
    <button
      {...props}
      classList={{
        "rounded leading-none transition outline-none focus:outline-none focus:ring focus:ring-secondary-1":
          true,
        "px-2 py-1": size === "small",
        "px-3 py-2": size === "regular",
        "bg-primary hover:bg-gray-200": kind === "primary",
        "bg-transparent hover:bg-gray-200": kind === "secondary",
      }}
    >
      {props.children}
    </button>
  );
};

export default Button;
