import { Component, createSignal, Index, mergeProps, Show } from "solid-js";
import { useNavigate } from "solid-app-router";
import { Icon } from "solid-heroicons";
import { search } from "solid-heroicons/outline";
import { gql } from "@urql/core";
import { useUrql } from "../../contexts/urql";
import SearchResult from "./search/SearchResult";
import SearchMessage from "./search/SearchMessage";
import {
  SearchTopics,
  SearchTopicsVariables,
  SearchTopics_topics,
} from "./__generated__/SearchTopics";

const SearchTopicsQuery = gql`
  query SearchTopics($query: String!) {
    topics(query: $query) {
      id
      content {
        id
        name
      }
    }
  }
`;

interface Props {
  size?: "small" | "regular";
}

const SearchInput: Component<Props> = (props) => {
  const merged = mergeProps({ size: "regular" }, props);
  const { urql } = useUrql();
  const navigateTo = useNavigate();
  const [topics, setTopics] = createSignal<Array<SearchTopics_topics>>([]);
  const [errorMessage, setErrorMessage] = createSignal<null | string>(null);
  const [selectedTopic, setSelectedTopic] = createSignal<number | null>(null);

  const resetTopics = () => {
    setTopics([]);
    setSelectedTopic(null);
    setErrorMessage(null);
  };

  const onSelectTopic = (topic: SearchTopics_topics) => {
    resetTopics();
    navigateTo(`/en/topic/${topic.content.id}`, { resolve: true });
  };

  const debounceInput = (func: (query: string) => unknown, delay = 300) => {
    let timeout: number;

    return (e: { currentTarget: HTMLInputElement }) => {
      const query = e.currentTarget.value;
      clearTimeout(timeout);
      timeout = setTimeout(() => func(query), delay);
    };
  };

  const onQuery = async (query: string) => {
    resetTopics();
    if (!query || query.length < 2) {
      return;
    }

    const result = await urql
      .query<SearchTopics, SearchTopicsVariables>(SearchTopicsQuery, { query })
      .toPromise();
    if (result.error) {
      setErrorMessage(result.error.message);
    } else if (result.data) {
      setTopics(result.data.topics);
    }
  };

  const onKeyDown = (e: KeyboardEvent) => {
    const selected = selectedTopic();
    if (topics().length === 0) {
      return;
    } else if (e.key === "ArrowDown") {
      e.preventDefault();
      if (selected === null) {
        setSelectedTopic(0);
      } else if (selected + 1 < topics().length) {
        setSelectedTopic(selected + 1);
      }
    } else if (e.key === "ArrowUp") {
      e.preventDefault();
      if (selected !== null && selected - 1 >= 0) {
        setSelectedTopic(selected - 1);
      }
    } else if (e.key === "Enter" && selected !== null) {
      onSelectTopic(topics()[selected]);
    }
  };

  const onKeyUp = (e: KeyboardEvent) => {
    if (e.key === "ArrowDown" || e.key === "ArrowUp" || e.key === "Enter") {
      e.preventDefault();
    }
  };

  return (
    <div class="relative">
      <div class="flex flex-wrap items-stretch">
        <span
          class="z-10 h-full leading-normal font-normal absolute text-center text-gray-400 rounded items-center justify-center w-8"
          classList={{
            "pl-3 py-3": merged.size === "regular",
            "pl-1 py-1": merged.size !== "regular",
          }}
        >
          <Icon path={search} class="h-5 inline p-0 m-0" />
        </span>
        <input
          autofocus
          autocomplete="off"
          type="text"
          placeholder="Search"
          class="placeholder-gray-400 text-dark bg-white rounded text-base border border-gray-400 outline-none transition focus:outline-none focus:ring focus:ring-secondary-1 w-full"
          classList={{
            "px-3 py-3 pl-10": merged.size === "regular",
            "px-1 py-1 pl-8": merged.size !== "regular",
          }}
          onkeydown={onKeyDown}
          onkeyup={onKeyUp}
          oninput={debounceInput(onQuery)}
        />
      </div>
      <Show when={errorMessage() !== null}>
        <SearchMessage message={errorMessage()!} />
      </Show>
      <Show when={topics().length > 0}>
        <ul class="absolute shadow-lg rounded bg-white text-dark w-full">
          <Index each={topics()}>
            {(item, index) => (
              <SearchResult
                name={item().content.name}
                localizedId={item().content.id}
                isSelected={index === selectedTopic()}
                onSelect={() => onSelectTopic(item())}
              />
            )}
          </Index>
        </ul>
      </Show>
    </div>
  );
};

export default SearchInput;
