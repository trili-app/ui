/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SearchTopics
// ====================================================

export interface SearchTopics_topics_content {
  __typename: "LocalizedTopic";
  id: string;
  name: string;
}

export interface SearchTopics_topics {
  __typename: "Topic";
  id: string;
  content: SearchTopics_topics_content;
}

export interface SearchTopics {
  topics: SearchTopics_topics[];
}

export interface SearchTopicsVariables {
  query: string;
}
