import { Component } from "solid-js";

interface Props {
  level: number;
}

const Header: Component<Props> = (props) => {
  if (props.level === 1) {
    return <h1 class="font-serif text-dark text-4xl mb-5">{props.children}</h1>;
  } else if (props.level === 2) {
    return <h2 class="font-serif text-dark text-2xl mb-3">{props.children}</h2>;
  } else if (props.level === 3) {
    return <h3 class="font-serif text-dark text-lg mb-2">{props.children}</h3>;
  } else if (props.level === 4) {
    return <h4 class="font-serif text-dark mb-1">{props.children}</h4>;
  }
  return <h5 class="font-bold text-dark mb-1">{props.children}</h5>;
};

export default Header;
