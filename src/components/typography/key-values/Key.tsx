import { Component } from "solid-js";

const Key: Component = (props) => {
  return <div class="col-span-2">{props.children}</div>;
};

export default Key;
