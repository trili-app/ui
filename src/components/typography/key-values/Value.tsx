import { Component } from "solid-js";

const Value: Component = (props) => {
  return <div class="col-span-4">{props.children}</div>;
};

export default Value;
