import { Component, JSX } from "solid-js";
import { Link, LinkProps } from "solid-app-router";

const LinkComponent: Component<LinkProps> = (props) => {
  return (
    <Link href={props.href} class="text-complementary">
      {props.children}
    </Link>
  );
};

export default LinkComponent;
