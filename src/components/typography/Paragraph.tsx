import { Component } from "solid-js";

const Paragraph: Component = (props) => {
  return <p class="mb-4 text-dark max-w-4xl text-justify">{props.children}</p>;
};

export default Paragraph;
