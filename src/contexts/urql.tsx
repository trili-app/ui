import { createContext, useContext, Component } from "solid-js";
import { Client, createClient } from "@urql/core";

export const UrqlContext = createContext<UrqlContext>();

interface UrqlContext {
  urql: Client;
}

export const UrqlProvider: Component = (props) => {
  const urql = createClient({
    url: "http://localhost:8080/graphql",
  });

  return (
    <UrqlContext.Provider value={{ urql: urql }}>
      {props.children}
    </UrqlContext.Provider>
  );
};

export const useUrql = () => useContext(UrqlContext)!;
