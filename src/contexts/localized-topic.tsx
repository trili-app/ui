import { createContext, useContext, Component } from "solid-js";
import { createStore, Store } from "solid-js/store";
import { LocalizedTopic_localizedTopic } from "../views/__generated__/LocalizedTopic";

export const LocalizedTopicContext = createContext<LocalizedTopicContext>();

export type Level = "beginner" | "advanced" | "expert";

type LocalizedTopicStore = {
  topic: LocalizedTopic_localizedTopic | null;
  level: Level;
};

type LocalizedTopicContext = [
  Store<LocalizedTopicStore>,
  {
    setTopic: (topic: LocalizedTopic_localizedTopic | null) => void;
    setLevel: (level: Level) => void;
  }
];

type Props = {
  topic: LocalizedTopic_localizedTopic | null;
};

const [store, setStore] = createStore<LocalizedTopicStore>({
  level: "beginner",
  topic: null,
});

export const LocalizedTopicProvider: Component<Props> = (props) => {
  return (
    <LocalizedTopicContext.Provider
      value={[
        store,
        {
          setTopic: (topic) => setStore({ topic }),
          setLevel: (level) => setStore({ level }),
        },
      ]}
    >
      {props.children}
    </LocalizedTopicContext.Provider>
  );
};

export const useLocalizedTopic = () => useContext(LocalizedTopicContext)!;
